
const express = require("express");
const app = express();
const port = 3000;


app.use(express.json())
app.use(express.urlencoded({extended: true}))


//1
app.get("/home", (req, res) => res.send(`Welcome to home page!`));

//2
app.get("/users", (req, res) => {
	res.send(req.body)
});

//3
app.delete("/delete-user", (req, res) => {
	let user = req.body.username;
	let pass = req.body.password;

	res.send(`User ${user} has been deleted.`)
});


app.listen(port, () => console.log(`Server running at port ${port}`));